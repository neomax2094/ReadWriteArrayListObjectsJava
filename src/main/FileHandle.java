package main;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class FileHandle {

    public ArrayList<Student> createStd(){
        Random rnd = new Random();
        ArrayList<Student> students = new ArrayList<>();
        for (int i=0; i<100; i++){
            students.add(new Student("x"+i+" ", rnd.nextInt(50)));
        }
        return students;
    }

    public void writeStd(ArrayList<Student> students){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("std.obj",true);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(students);
            objectOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Student> readStd(){
        ArrayList<Student> students = null;
        try {
            FileInputStream fileInputStream = new FileInputStream("std.obj");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
//            for (int i=0; i<objectInputStream.readInt(); i++){
//                Student s = (Student) objectInputStream.readObject();
//                students.add(s);
//            }
            students = (ArrayList<Student>)objectInputStream.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return students;
    }
}
